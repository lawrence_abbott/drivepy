#! /usr/bin/python
# Main Frame - This is the root of everything, called from the App in main.py
# Time-stamp: "2008-05-15 16:28:18 jantman"
# $Id: wxmain.py,v 1.8 2008/05/15 20:32:43 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

# DrivePy is a wxPython project and a fork of Jason Antman's TuxTruck . It uses TuxTruck as it's base but changes are such that it
# is incompatible with the original project.  It is very much alpha code, but the audio player section is working fine,
# with the proviso's detailed in the code. Contact Lawrie at lawabb@gmail.com for any questions.

# TODO : Use parapin parallel port input from Headlight switch to
# switch day/night automatically
# http://parapin.sourceforge.net/
# or pyparallel

module = 'DrivePy'

import sys, os
sys.path.append(os.getcwd())

if not hasattr(sys, 'frozen'):
	import wxversion
	#wxversion.select('2.8')

import wx

# local imports
import AudioPanelMain, Settings, HomePanelClock, Toolbar, Playlist,\
AudioPlayerPanel, GPSPanel, OBDPanel, PhonePanel, ConfigPanel

#import rana
#import powermon
#gps = '/home/lawrie/src/rana'


class DrivePy(wx.App):
    def OnInit(self):
        frame = Main("", (0,0))
        frame.Show()
        self.SetTopWindow(frame)

        ## start the power monitoring - if power fails shutdown app then os
        #frame.PowermonStart()

        return True

class Main(wx.Frame):
    """
    This is the top-level frame. It's the root of everything and everything happens (or is started here).
    This should just handle building the base GUI (blank main panel), and then instantiate
    child classes to do EVERYTHING else. Each part of the GUI should be its own class, that
    holds a main panel and then does everything relating to that component (hopefully with
    multiple child classes).
    NOTE: This should ONLY
      a) start the GUI, and init everything
      b) init Settings to get user settings
      c) init any of the elements/categories that need to be constantly running (gps, phone, audio, obd)
      d) handle ALL of the communication/events that require interaction between categories, or
         require interrupts (GPS instructions, pop-ups, phone calls, etc.)
    """

    _currentColorScheme = 'night'

    def __init__(self, parent, id):
        """
        This is the BIG function. It initiates EVERYTHING that gets initiated at start,
        including settings, and all components that must run as long as the app is running.
        It SHOULD initiate GPS, Phone, and anything else that could take a while to start,
        as soon as possible. It also initiates everything that must run constantly.
        """
        wx.Frame.__init__(self, None, -1, '', style=wx.NO_BORDER)

        self.audio_click = -1 ### dont change this ###

        self.gps_running = False

        #self.gps_running  = True
        #self.gps = rana.GuiBase('carpc')
        #self.gps.hide()

        # setup the settings. All setings accessed through here for every panel
        self.settings = Settings.Settings(module)

        # setup the main frame
        self.SetPosition(self.settings.skin.topWindowPos) # set the main window position
        self.SetSize(self.settings.skin.topWindowSize)        # set the main window size
        if self.settings.skin.topWindowCentered == 1:
            # check whether to center the window or not
            self.CenterOnScreen()
        self.SetWindowStyle(wx.NO_BORDER) # set window style to have no border

        # add and init panels
        self.toolbar = Toolbar.Toolbar(self, -1)
        self.homeClock = HomePanelClock.HomePanel_Clock(self, -1)
        self.GPS = GPSPanel.GPSPanel(self, -1)
        self.audioMain = AudioPanelMain.AudioPanel_Main(self, -1)
        self.audioPlayer = AudioPlayerPanel.AudioPlayerPanel(self, -1)
        self.OBD = OBDPanel.OBDPanel(self, -1)
        self.Phone = PhonePanel.PhonePanel(self, -1)
        self.Config = ConfigPanel.ConfigPanel(self, -1)
        #self.Weather = WeatherPanel.WeatherPanel(self, -1)

        # Create a list of Panels we have  for use later
        self.panels = [self.homeClock, self.audioMain, self.audioPlayer, self.GPS, self.OBD,
        #self.Phone, self.Config, self.Weather]
        self.Phone, self.Config]

        # now SET THE SKINS on EVERYTHING
        self.reSkin(self._currentColorScheme)

        # Set which is initial Audio panel
        self.audio_panel_is_player = 1
        self.audioPlayer.Start()  # TODO - this needs to be a selectable option ie Autostart- checkbox -yes
        self.OnClick_audio(None)  # Set the default panel to show
        #self.OnClick_home(None)

    def PowermonStart(self):
        self.pmon = powermon.PowerMon(self)
        self.pmon.setDaemon(True)
        self.pmon.start()

    def PowermonStop(self):
        self.pmon.join()

    def UpdateDirs(self):
        ''' Update mp3 locations file. Run this  manually when song collection is updated'''
        self.audioPlayer.UpdateDirs()

    def PlayNext(self, event):
        self.audioPlayer.OnPlayNext(None)

    def OnClick_home(self, event):
        """ Handles click of the home button, switching to the home screen"""
        #self.toolbar._currentButton = self.toolbar.butn_home
        self.switchToModePanel(self.homeClock)

    def OnClick_gps(self, event):
        """ Handles click of the GPS button, switching to the GPS screen"""
        #self.toolbar._currentButton = self.toolbar.butn_gps
        self.switchToModePanel(self.GPS)


    def OnClick_audio(self, event):
        """
        Handles click of the Audio button, switching to the audio screen
        On the first click of the button, status should remain at default. On second and subsequent clicks, should
        toggle between player and main panels. If another button is pressed audio_click is reset to -1 so no
        toggle next time audio button is pressed (until second click)
        """
        #self.toolbar._currentButton = self.toolbar.butn_audio

        self.audio_click +=1
        if self.audio_click > 1:
            self.audio_click = 1

        if self.audio_click:
            self.audio_panel_is_player = not self.audio_panel_is_player

        if self.audio_panel_is_player:
            self.switchToModePanel(self.audioPlayer)
        else:
            self.switchToModePanel(self.audioMain)

    def OnClick_obd(self, event):
        """Handles click of the OBD button, switching to the OBD screen"""
        #self.toolbar._currentButton = self.toolbar.butn_obd
        #print"drivepy 148: ",  self.toolbar.button.GetId() # DEBUG
        #print"drivepy 149: ",  event.GetId()                       # DEBUG
        self.switchToModePanel(self.OBD)

    def OnClick_phone(self, event):
        """ Handles click of the phone button, switching to the phone screen"""
        #self.toolbar._currentButton = self.toolbar.butn_phone
        self.switchToModePanel(self.Phone)

    def OnClick_tools(self, event):
        """Handles click of the tools button, switching to the tools screen"""
        #self.toolbar._currentButton = self.toolbar.butn_tools
        self.switchToModePanel(self.Config)

    def OnClick_weather(self, event):
        """Handles click of the weather button, switching to the weather screen"""
        #self.toolbar._currentButton = self.toolbar.butn_weather
        self.switchToModePanel(self.Weather)

    def switchColorScheme(self):
        """ This method  toggles between day/night modes"""

        if self._currentColorScheme == "day":
            self._currentColorScheme = 'night'
            self.reSkin("night")
        elif self._currentColorScheme == "night":
            self._currentColorScheme = 'day'
            self.reSkin("day")

    def reSkin(self,  color):
        """ Set colour scheme to day or night for all"""

        #reSkin frame
        self.SetBackgroundColour(getattr(self.settings.skin, color +'_bgColor'))

        # reSkin all the panels
        [getattr(panel, 'reSkin')(self._currentColorScheme) for panel in self.panels]

        # reSkin the toolbar
        self.toolbar.reSkin(self._currentColorScheme)

        self.Refresh()
        self.ToolbarRefresh()

    def switchToModePanel(self, activePanel):
        """
        Hides all of the top-level mode panels and shows the one we want
        """

        if not (bool(activePanel == self.audioPlayer) or bool(activePanel == self.audioMain)):
            self.audio_click = -1

        for panel in self.panels:
            if activePanel == panel:
                getattr(panel, 'Show')()
            else:
                getattr(panel, 'Hide')()

        self.ToolbarRefresh()

    def ToolbarRefresh(self):
        self.toolbar.ToolbarRefresh()

    def Quit(self):
        print "drivepy: Quit"
        self.audioPlayer.CloseMplayer()
        self.Destroy()


if __name__ == '__main__':
    """
    Main method for the whole program. This gets called when we start this application,
    and it instantiates all of the necessary classes and starts the GUI and backend code.
    """
    app = DrivePy(False)
    app.MainLoop()