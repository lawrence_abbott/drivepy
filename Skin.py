# TuxTruck Skin Manager
# Time-stamp: "2008-05-08 20:50:44 jantman"
# $Id: TuxTruck_Skin_DigitalClock.py,v 1.2 2008/05/09 01:22:06 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

import wx, os
import xml.etree.ElementTree as ElementTree

# local imports
import Utility

class Button:
    """
    This handles all of the skin parsing and variables for the buttons.
    It is instantiated in Settings.SkinManager and all calls should be from there.
    """
    
    def __init__(self, parent, file):
        """
        Here, we get the default skin name from settings, then load that file.
        This must happen before we build any of the GUI.
        """
        self.loadSkin( file) # load the file
        
    def loadSkin(self,  file):
        """
        This function loads a new skin by reading and parsing the file, and then
        updating the variables in this class. The skin is specified by a file, in the
        path specified in the class documentation. It only updates the variables here,
        and doesn't make any changes to the GUI.
        NOTE: The size of buttons is exactly the size of the image used for the button.
        """
        #print "Skin: Button class - load skin -refactoring"
        items = ['home', 'gps', 'audio', 'obd', 'phone', 'tools', 'weather']
        tod = ['day', 'night']
        actv = ['active', '' ]
        
        # TODO: parse the XML and read the values
        skinTree = ElementTree.parse(file).getroot()

        # parse the window information
        buttonTree = skinTree.find('button_images')

        self.width = int(buttonTree.findtext("image_width"))
        self.height = int(buttonTree.findtext("image_height"))
        '''
        for x in actv:
            for y in tod:
                for z in items:
                    print buttonTree.findtext('_'.join([y, z, x]).rstrip('_'))
                    'self'+ '_'.join([y, z, x]).rstrip('_')
                    

        '''
        self.day_home = buttonTree.findtext("day_home")
        self.day_gps = buttonTree.findtext("day_gps")
        self.day_audio = buttonTree.findtext("day_audio")
        self.day_phone = buttonTree.findtext("day_phone")
        self.day_weather = buttonTree.findtext("day_weather")
        self.day_obd = buttonTree.findtext("day_obd")
        self.day_tools = buttonTree.findtext("day_tools")
        
        self.day_home_active = buttonTree.findtext("day_home_active")
        self.day_gps_active = buttonTree.findtext("day_gps_active")
        self.day_audio_active = buttonTree.findtext("day_audio_active")
        self.day_phone_active = buttonTree.findtext("day_phone_active")
        self.day_weather_active = buttonTree.findtext("day_weather_active")
        self.day_obd_active = buttonTree.findtext("day_obd_active")
        self.day_tools_active = buttonTree.findtext("day_tools_active")
        
        self.night_home = buttonTree.findtext("night_home")
        self.night_gps = buttonTree.findtext("night_gps")
        self.night_audio = buttonTree.findtext("night_audio")
        self.night_phone = buttonTree.findtext("night_phone")
        self.night_weather = buttonTree.findtext("night_weather")
        self.night_obd = buttonTree.findtext("night_obd")
        self.night_tools = buttonTree.findtext("night_tools")
        
        self.night_home_active = buttonTree.findtext("night_home_active")
        self.night_gps_active = buttonTree.findtext("night_gps_active")
        self.night_audio_active = buttonTree.findtext("night_audio_active")
        self.night_phone_active = buttonTree.findtext("night_phone_active")
        self.night_weather_active = buttonTree.findtext("night_weather_active")
        self.night_obd_active = buttonTree.findtext("night_obd_active")
        self.night_tools_active = buttonTree.findtext("night_tools_active")
        
class AnalogClock:
    """
    This class handles skin settings for the analog clock
    """
    
    def __init__(self, parent, file):
        """
        Here, we get the default skin name from settings, then load that file.
        This must happen before we build any of the GUI.
        """
        self.loadSkin(file) # DEBUG - everything else
        
    def loadSkin(self, file):
        """
        This function loads a new skin by reading and parsing the file, and then
        updating the variables in this class. The skin is specified by a file, in the
        path specified in the class documentation. It only updates the variables here,
        and doesn't make any changes to the GUI.
        NOTE: The size of buttons is exactly the size of the image used for the button.
        """

        skinTree = ElementTree.parse(file).getroot()
        analogClockTree = skinTree.find('analogClock')

        # analog clock settings
        self.day_handColor = Utility.str2tuple(analogClockTree.findtext("day_handColor"), "analogClock.day_handColor")
        self.day_shadowColor = Utility.str2tuple(analogClockTree.findtext("day_shadowColor"), "analogClock.day_shadowColor")
        self.day_bgColor = Utility.str2tuple(analogClockTree.findtext("day_bgColor"), "analogClock.day_bgColor")
        self.day_tickColor = Utility.str2tuple(analogClockTree.findtext("day_tickColor"), "analogClock.day_tickColor")
        self.day_faceColor = Utility.str2tuple(analogClockTree.findtext("day_faceColor"), "analogClock.day_faceColor")
        self.day_faceBorderColor = Utility.str2tuple(analogClockTree.findtext("day_faceBorderColor"), "analogClock.day_faceBorderColor")

        self.night_handColor = Utility.str2tuple(analogClockTree.findtext("night_handColor"), "analogClock.night_handColor")
        self.night_shadowColor = Utility.str2tuple(analogClockTree.findtext("night_shadowColor"), "analogClock.night_shadowColor")
        self.night_bgColor = Utility.str2tuple(analogClockTree.findtext("night_bgColor"), "analogClock.night_bgColor")
        self.night_tickColor = Utility.str2tuple(analogClockTree.findtext("night_tickColor"), "analogClock.night_tickColor")
        self.night_faceColor = Utility.str2tuple(analogClockTree.findtext("night_faceColor"), "analogClock.night_faceColor")
        self.night_faceBorderColor = Utility.str2tuple(analogClockTree.findtext("night_faceBorderColor"), "analogClock.night_faceBorderColor")

### None below in use ###

class ExternalThermo:
    """
    This class handles skin settings for the external thermometer
    """
    
    def __init__(self, parent, file):
        """
        Here, we get the default skin name from settings, then load that file.
        This must happen before we build any of the GUI.
        """
        self.loadSkin(file)
        
    def loadSkin(self, file):
        """
        This function loads a new skin by reading and parsing the file, and then
        updating the variables in this class. The skin is specified by a file, in the
        path specified in the class documentation. It only updates the variables here,
        and doesn't make any changes to the GUI.
        NOTE: The size of buttons is exactly the size of the image used for the button.
        """

        skinTree = ElementTree.parse(file).getroot()
        externalThermoTree = skinTree.find('externalThermo')

        self.day_fgColor = Utility.str2tuple(externalThermoTree.findtext("day_fgColor"), "externalThermo.day_fgColor")
        self.day_bgColor = Utility.str2tuple(externalThermoTree.findtext("day_bgColor"), "externalThermo.day_bgColor")
        self.day_fadeColor = Utility.str2tuple(externalThermoTree.findtext("day_fadeColor"), "externalThermo.day_fadeColor")
        self.night_fgColor = Utility.str2tuple(externalThermoTree.findtext("night_fgColor"), "externalThermo.night_fgColor")
        self.night_bgColor = Utility.str2tuple(externalThermoTree.findtext("night_bgColor"), "externalThermo.night_bgColor")
        self.night_fadeColor = Utility.str2tuple(externalThermoTree.findtext("night_fadeColor"), "externalThermo.night_fadeColor")
        self.fadeFactor = int(externalThermoTree.findtext("fadeFactor"))

class InternalThermo:
    """
    This class handles skin settings for the internal thermometer
    """
    
    def __init__(self, parent, file):
        """
        Here, we get the default skin name from settings, then load that file.
        This must happen before we build any of the GUI.
        """
        self.loadSkin(file)

    def loadSkin(self, file):
        """
        This function loads a new skin by reading and parsing the file, and then
        updating the variables in this class. The skin is specified by a file, in the
        path specified in the class documentation. It only updates the variables here,
        and doesn't make any changes to the GUI.
        NOTE: The size of buttons is exactly the size of the image used for the button.
        """

        skinTree = ElementTree.parse(file).getroot()
        internalThermoTree = skinTree.find('internalThermo')

        self.day_fgColor = Utility.str2tuple(internalThermoTree.findtext("day_fgColor"), "internalThermo.day_fgColor")
        self.day_bgColor = Utility.str2tuple(internalThermoTree.findtext("day_bgColor"), "internalThermo.day_bgColor")
        self.day_fadeColor = Utility.str2tuple(internalThermoTree.findtext("day_fadeColor"), "internalThermo.day_fadeColor")
        self.night_fgColor = Utility.str2tuple(internalThermoTree.findtext("night_fgColor"), "internalThermo.night_fgColor")
        self.night_bgColor = Utility.str2tuple(internalThermoTree.findtext("night_bgColor"), "internalThermo.night_bgColor")
        self.night_fadeColor = Utility.str2tuple(internalThermoTree.findtext("night_fadeColor"), "internalThermo.night_fadeColor")
        self.fadeFactor = int(internalThermoTree.findtext("fadeFactor"))

class DigitalClock:
    """
    This class handles skin settings for the digital clock
    """
    
    def __init__(self, parent, file):
        """
        Here, we get the default skin name from settings, then load that file.
        This must happen before we build any of the GUI.
        """
        self.loadSkin(file)

    def loadSkin(self, file):
        """
        This function loads a new skin by reading and parsing the file, and then
        updating the variables in this class. The skin is specified by a file, in the
        path specified in the class documentation. It only updates the variables here,
        and doesn't make any changes to the GUI.
        NOTE: The size of buttons is exactly the size of the image used for the button.
        """

        skinTree = ElementTree.parse(file).getroot()
        digitalClockTree = skinTree.find('digitalClock')

        self.day_fgColor = Utility.str2tuple(digitalClockTree.findtext("day_fgColor"), "digitalClock.day_fgColor")
        self.day_bgColor = Utility.str2tuple(digitalClockTree.findtext("day_bgColor"), "digitalClock.day_bgColor")
        self.day_fadeColor = Utility.str2tuple(digitalClockTree.findtext("day_fadeColor"), "digitalClock.day_fadeColor")
        self.night_fgColor = Utility.str2tuple(digitalClockTree.findtext("night_fgColor"), "digitalClock.night_fgColor")
        self.night_bgColor = Utility.str2tuple(digitalClockTree.findtext("night_bgColor"), "digitalClock.night_bgColor")
        self.night_fadeColor = Utility.str2tuple(digitalClockTree.findtext("night_fadeColor"), "digitalClock.night_fadeColor")
        self.fadeFactor = int(digitalClockTree.findtext("fadeFactor"))

 
