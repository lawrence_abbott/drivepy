#! /usr/bin/env python
# Audio main frame
# Time-stamp: "2008-05-27 13:10:50 jantman"
# $Id: AudioPanel_Main.py,v 1.8 2008/05/27 18:23:08 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

import wx

class AudioPanel_Main(wx.Panel):
    """
    This is the top-level panel for audio functions, including audio file playing, radio, and podcast playing.
    """

    def __init__(self, parent, id):
        """
        Init function for the audio panel. Sets everything up.
        """
        self.parent = parent
        self.settings = parent.settings
        
        wx.Panel.__init__(self, parent, id)

        # setup the panel
        self.SetPosition(wx.Point(0, 0))
        x, y  =  self.settings.skin.topWindowSize     
        self.SetSize(wx.Size(x, y - self.settings.skin.butn.height))

        # test button - being used to generate mp3 directories file
        self.button1 = wx.Button(self, -1, "Generate dirs list")
        self.button1.SetPosition(wx.Point(300, 100))
        self.button1.SetSize((100, 50))
        self.button1.Bind(wx.EVT_BUTTON, self.test) # DEBUG

        self.Hide()
        
    def reSkin(self, color):
        # re-skin me
        self.SetBackgroundColour(getattr(self.settings.skin, color +'_bgColor'))
        self.Refresh()
        
    def test(self, event):
        print "Writing new list of mp3 directorys"
        self.parent.UpdateDirs()
