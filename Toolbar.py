#! /usr/bin/env python
# TuxTruck Main Frame Toolbar
# Time-stamp: "2008-05-12 15:23:15 jantman"
# $Id: TuxTruck_Toolbar.py,v 1.7 2008/05/12 19:22:43 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

import wx, os
from path import path

class Toolbar(wx.Panel):
    """
    This is the main toolbar for TuxTruck, appearing on every screen.
    """

    _currentButton = "" # reference to the currently selected button

    def __init__(self, parent, id):
        """
        Init the toolbar, set size and position, set it visible, call reSkin to setup the skin,
        finally bind all buttons to methods in parent (TuxTruck_Main)
        """
        wx.Panel.__init__(self, parent, id) # init the panel

        self.settings = parent.settings
        self.buttons_list = []
        self.button_list = ['home', 'gps', 'audio', 'obd', 'phone', 'tools'] # deleted 'weather'
        
        # Calculate position of toolbar
        sizex, sizey = self.settings.skin.topWindowSize
        
        pos_x = ((sizex/2)-((len(self.button_list)* self.settings.skin.butn.width)/2))
        pos_y = sizey - self.settings.skin.butn.height
        
        self.SetPosition(wx.Point(pos_x, pos_y)) # set the main window position
        self.SetSize(wx.Size(sizex, self.settings.skin.butn.height))
        
        self.box = wx.BoxSizer(wx.HORIZONTAL) # TODO: give this a meaningful name
    
        # Create each of the buttons 
        size = (self.settings.skin.butn.width, self.settings.skin.butn.height)
        
        def Buttons(par, handler, size, slabel, index):
            self.button = wx.BitmapButton(par, id=index, size=size)
            self.button.Bind(wx.EVT_BUTTON, handler)
            self.do_button_images(slabel)
            self.box.Add(self.button, proportion=0)
            self.buttons_list.append(self.button)

        for index, slabel in enumerate(self.button_list):
            handler = getattr(parent, 'OnClick_'+ slabel)
            Buttons(self, handler, size, slabel, index)
        
        self.SetAutoLayout(True)
        self.SetSizer(self.box)
        self.Layout()

        #self._currentButton = self.butn_home # set butn_home to be our initial button
        self.Show()
    
    def do_button_images(self,  label, color='night') :
        
        imggif = getattr(self.settings.skin.butn, color +'_'+ label)
        imgp= os.path.join(os.path.expanduser('~'), self.settings.skin._buttonImagePath)
        #imgpt = os.path.join(imgp, self.settings.skin._buttonImagePath)
        imgpth = path.joinpath(path(imgp), imggif)
        
        # TODOD imgpth user dir ifo should go in config file
        self.button.SetBitmapLabel( wx.Image(imgpth, wx.BITMAP_TYPE_ANY).ConvertToBitmap())
    
    def SetButtonImages(self, color):
        '''
        This method sets the images of all of the buttons to the correct images
        for the selected color scheme (day/night within a specific skin). 
        '''
        for index, slabel in enumerate(self.button_list):
            self.button = next((button for button in self.buttons_list if button.GetId() == index), None) 
            self.do_button_images(slabel, color)
            
    def reSkin(self, color):
        '''
        Re-load skin information for specified colorSchemeName (day|night)
        '''
        self.SetButtonImages(color)
        self.SetBackgroundColour(getattr(self.settings.skin, color +'_bgColor'))
        self.Refresh()
    
    def ToolbarRefresh(self):
        self.Refresh()
    