#! /usr/bin/env python
# Audio Player
# Time-stamp: "2008-05-19 00:27:24 jantman"
# $Id: AudioPlayer.py,v 1.7 2008/05/19 06:08:50 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

import wx, sys, os, time, subprocess

# local imports
import ContinuousTimer

STATUS_TIMEOUT = 0.8   

class AudioPlayer():
    """
    This is the audio player. It handles playing audio files, podcasts, playlists, etc.
    """

    # local variables to store state
    _currentSongPath = ""
    _currentSongLength = 0
    _currentSongName = ""
    _currentSongArtist = ""
    _currentSongAlbum = ""

    __currentlyPlaying = False # this is used to tell whether to first call closeMplayer() when play() is called.
    # closeMplayer() should be called whenever we're in the middle of a job.
    

    def __init__(self, parent, id):

        self.parent = parent
        self.result = 1
        self.process = self.mplayerIn = self.mplayerOut = None
        self.EOF = self.statusQuery = 0
        self.paused = False
        self.statusQuery = ContinuousTimer.ContinuousTimer(self, self.queryStatus, STATUS_TIMEOUT) 

    def play(self, target):
        """
        This function handles playing of a file. All play requests should come through here. It quits the currently running session, if needed, cleans up, and plays a file.
        """
        
        if self.__currentlyPlaying == True:
            # if currently playing, quit and cleanup
            self.closeMplayer()

        self.EOF = self.seconds = self.paused = 0
        self.parent.updateProgressBar(0)
        self.parent.Refresh()
        
        try:
            # check to make sure the file exists. if so, play it
            if os.path.exists(target):
                self._currentSongPath = target # update the variable holding the current path
                
                # idle option may not be required - msglevel 6 enables EOF message
                cmd = "mplayer -slave -quiet -idle -nolirc -msglevel global=6  \"%s\" 2>/dev/null" % target 
                
                self.process = wx.Process()
                self.process.Redirect()

                self.pid = wx.Execute(cmd, wx.EXEC_ASYNC, self.process)
                    
                self.mplayerIn = self.process.GetOutputStream()
                self.mplayerOut = self.process.GetInputStream()
                
                # Use wx.FutureCall to deal with mplayer startup bottleneck
                wx.FutureCall(500, self.GetSongLength)
                wx.FutureCall(800, self.startStatusQuery)

                self.__currentlyPlaying = True
                
            else:
                print "AudioPlayer: File %s does not exist, not playing." % target
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise
            
    def GetPlayStatus(self):
        '''This is info for GUI play control'''
        return self.__currentlyPlaying
    
    def GetSongLength(self):
        """This gets  song  length, to set gauge length"""
        
        line, seconds, result = None, -1, 0
        
        self.mplayerIn.write("get_time_length\n")
        time.sleep(0.01)  #allow time for output
            
        while not result:
            try:  #attempt to fetch last line of output
                line = self.mplayerOut.readline()
            except StandardError:
                break

            if not line:
                break

            if line.startswith("ANS_LENGTH"):
                result = 1
                seconds = float(line.replace("ANS_LENGTH=", ""))
                self._currentSongLength = seconds
            else:
                time.sleep(0.005)
                    
        self.parent.SetSongLength(self._currentSongLength) # set gauge range


    def cmd(self, command):
        """
        This function handles sending commands to MPlayer.
        """
        if not self.mplayerIn:
            return
        try:
            self.mplayerIn.write(command + "\n")
        except StandardError:
            return
        
    def pause(self):
        """
        This function toggles pausing of the current mplayer job, as well as the statusQuery timer.
        """
        if not self.mplayerIn:
            return
        # Info: self.cmd("get_property pause") 
        if self.paused:  #unpause
            self.cmd("pause")
            self.startStatusQuery()
            self.paused = False
        else:               #pause
            self.stopStatusQuery()
            self.cmd("pause")
            self.paused = True
            
    def seek(self, amount, mode=0):
        """This seeks the specified amount in the current file
        """
        self.cmd("seek " + str(amount) + " " + str(mode))
    
    def queryStatus(self):
        """
        This function queries MPlayer's status when running and updates the progress bar on the parent panel.
        """
        if self.result:
            # if got previous answer already, can ask for new time info
            self.cmd("get_time_pos") 
            time.sleep(0.01)  #allow time for output
            self.result = 0
        else:
            # did not get an answer , make out we did and try again next timer cycle
            self.result=1
            return
            
        line, seconds = None, -1
        
        while not self.result:
            #attempt to fetch last line of output
            try:  
                line = self.mplayerOut.readline()
                #print 'line ', line # DEBUG
            except StandardError:
                break
                
            if line.startswith("EOF"): # End of File - Song finished
                    self.EOF = 1
                    self.EOFhandler()
                    break
            elif  line.startswith("ANS_TIME_POSITION"):
                seconds = float(line.replace("ANS_TIME_POSITION=", ""))
                self.result = 1
            else:
                # keep reading
                # this has to be low enough to enable dealing with the load of messages on corrupt mp3s
                # but make it > zero to protect from CPU hogging
                time.sleep(0.005)
        
        if seconds > 0:
            self.seconds = seconds
        #print self.seconds
        self.parent.updateProgressBar(self.seconds)
            
        return True
    
    
    def EOFhandler(self):
        '''
        End of file normally triggered by mplayer stdout message EOF (use msglevel 6). 
        wx.CallAfter being used as a simple way of doing wx.PostEvent. Done because otherwise Windows
        complains that mplayer is not being started from main thread...even though it will start.
        '''
        self.closeMplayer()
        wx.CallAfter(self.parent.OnPlayNext, None)

    def startStatusQuery(self):
        """
        This function starts the statusQuery timer.
        """
        # Moved to init - timer doesn't seem to need be init every song
        #self.statusQuery = ContinuousTimer.ContinuousTimer(self, self.queryStatus, STATUS_TIMEOUT) 
        self.statusQuery.start()

    def stopStatusQuery(self):
        """
        This function stops the statusQuery timer
        """
        if self.statusQuery:
            self.statusQuery.stop()
            #self.statusQuery = None

    def closeMplayer(self):
        """
        This cleanly closes any IPC resources to mplayer. It is called when playback finishes, as well as
        when starting playback of a new file when mplayer is in the middle of a job.
        """
        if self.paused:  #untoggle pause to cleanly quit
            self.pause()
        
        self.stopStatusQuery()  #cancel query
        #self.cmd("quit")  #ask mplayer to quit
            
        try:			
            if self.process is not None:
                self.cmd('quit')
                self.process.Detach()
                self.process.CloseOutput()
        except StandardError:
            print "AudioPlayer: Mplayer close error"
            pass
        try:
            self.process, self.mplayerIn, self.mplayerOut = None, None, None
        except:
            pass
            
        self.__currentlyPlaying = False


        # reset all local variables
        self._currentSongPath = ""
        self._currentSongLength = 0
        self._currentSongName = ""
        self._currentSongArtist = ""
        self._currentSongAlbum = ""
