# README #
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

# DrivePy is a wxPython project and a fork of Jason Antman's TuxTruck . It uses TuxTruck as it's base but changes are such that it
# is incompatible with the original project.  It is very much alpha code, but the audio player section is working fine,
# with the proviso's detailed in the code. Contact Lawrie at lawabb@gmail.com 