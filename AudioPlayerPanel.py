#! /usr/bin/env python
# Audio Player frame
# Time-stamp: "2008-05-27 13:11:24 jantman"
# $Id: AudioPlayerPanel.py,v 1.14 2008/05/27 18:23:08 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

# TODO - add buttons to skin mechanism


import wx, os, pickle, urllib, sys
from path import path

# local imports
import Playlist, AudioPlayer


class AudioPlayerPanel(wx.Panel):
    """
    This is the audio player panel. It gives us an interface for playing audio files
    Only one play mode at possible at present...
    Uses directory scroll up, down and across(cycle) to scroll through coverart. Current song continues to play - no change to playlist.
    When desired album is found, hit cover image to: created dict_playlist and start playing first song,
    create GUI playlist, and save list to currentPlaylist.xspf .
    Start directory should be that of current song beng played. Unfortunately - current code relies on VERY strict format of
    song titles and directory structure - does not use id tags

    eg first song (01) first directory would be

    <mp3root>/A/<artist>/<album>/01- <optional - > <song_title>.mp3

    and in that directory must be coverart file  'cover.jpg'

    Note that, where it matters, path separators are maintained as forward slashes. This helps avoid the bedlam of esc(\) vs path
    separator(\) on Windows when manipulating paths as strings. Windows accepts either slash as path separator.
    Note also - @ Directory has been added - this directory is for compliations - as @ is ascii 1 less than A
    - it seems as good as any dir name
    """


    def __init__(self, parent, id):
        """Init function for the audio player panel."""

        self.parent = parent
        wx.Panel.__init__(self, parent, id) # init the panel

        self.settings = parent.settings
        self.mplayer = AudioPlayer.AudioPlayer(self, -1)

        self._currentPlaylistPosition = self.BrowseDirIndex = 0 #overridden when values read from rc file
        self.BrowseDir = None
        self.revert_time = 30000 # milliseconds

        self.mp3root = self.settings.audio.mp3root

        # location of user  platform specific items
        self.userdir = self.settings.userdir
        self.rcdir = self.settings.rcdir

        self.playpos_rc = os.path.join(self.rcdir, 'playpos.rc')
        self.playdirs_rc = os.path.join(self.rcdir, 'playdirs.rc')

        # get the playlist. Playlist is then stored in dictionary
        proot = path.joinpath(path(self.rcdir), self.settings.audio.playlistroot)
        self.PL = Playlist.Playlist(self, proot)
        self.playlist_dict = self.PL.Read_xspf_Playlist()

        self._currentPlaylistDirs = []
        try:
            f = open(self.playdirs_rc, 'r')
            self._currentPlaylistDirs = pickle.load(f)
            f.close()
        except:
            print 'Unable to read playlist dirs'

        # setup the panel
        self.SetPosition(wx.Point(0, 0))
        x, y  =  self.settings.skin.topWindowSize
        x, y = (x, y - self.settings.skin.butn.height)
        self.SetSize(wx.Size(x, y))

        # Gauge to show song time elapsed/remaining
        goffset = 20
        self.gauge1 = wx.Gauge(self, -1)
        self.gauge1.SetSize((x - 2*goffset,8))
        self.gauge1.SetPosition((goffset, 48))
        self.gauge1.SetRange(1)

        # Add Album art from file - when hit he button show 'next' album cover and playlist
        bmp_pos = (8, 72)
        self.bmp_size = (330, 330)
        img =  "cover.jpg"
        print os.path.join(self.rcdir, img)
        image = wx.Image(os.path.join(self.rcdir, img)).Rescale(self.bmp_size[0], self.bmp_size[1])
        bmp = wx.BitmapFromImage(image)
        self.coverart = wx.BitmapButton(self, -1, bitmap=wx.NullBitmap, pos=bmp_pos, size=self.bmp_size)
        self.coverart.Bind(wx.EVT_BUTTON, self.OnCoverSelect)

        # display the playlist in a list box.
        lb_pos = (345, 58)
        self.lb = wx.ListBox(self, -1, lb_pos, (x- lb_pos[0], 350), ['',], wx.LB_SINGLE)
        self.lb.Bind(wx.EVT_LISTBOX, self.EvtListBox)
        self.lb.SetFont(wx.Font(20, wx.SWISS, wx.NORMAL, wx.BOLD))
        self.lb.SetFirstItem(self._currentPlaylistPosition)
        self.lb.SetSelection(self._currentPlaylistPosition)
        self.lb.SetItemForegroundColour(self._currentPlaylistPosition, 'blue')

        # buttons to control mplayer
        offset = 20
        buttonx = (x - 2*offset)/ 8
        buttony = 5
        button_size = (buttonx, 40)

        def BmpButton(parent, handler, image, pos, size):
            self.button = wx.BitmapButton(parent, -1, pos=pos, size=size)
            self.button.Bind(wx.EVT_BUTTON, handler)
            self.button.SetBitmapLabel(wx.Image(self.userdir+"/"+self.settings.skin._buttonImagePath+image).ConvertToBitmap())

        buttons = [(self.OnPlayPrev, 'rewind.gif'),
                                (self.OnPlayPause, 'play.gif'),
                                (self.OnPlayNext, 'ffwd.gif'),
                                (self.OnSkipRev, 'skiprev.gif'),
                                (self.OnSkipFwd, 'skipfwd.gif'),
                                (self.OnUpDir, 'up.gif'),
                                (self.OnDownDir, 'down.gif'),
                                (self.OnNextDir, 'next.gif')]

        [BmpButton(self, handler, image, (offset+i*buttonx , buttony), button_size)  for i, (handler, image) in enumerate(buttons)]

        # Set start path
        dir, file, filepath = self.dict_path()
        self.BrowseDir = dir
        self.BrowseDirIndex, item = next(((i, x) for i, x in enumerate(self._currentPlaylistDirs) if x == path(dir)), (None, None))

        # Set Album Cover
        self.SetImage(dir)
        self.SetPlaylistBox()

        # init timer - used to revert coverart on non-selection
        self.timer = wx.Timer(self)
        self.Bind( wx.EVT_TIMER, self.on_timer, self.timer)

        self.Hide()  # Show is done from main

    def on_timer(self, event):
        """revert to current playing coverart and index"""
        dir = path(self.tgt).splitpath()[0]
        self.BrowseDir = dir
        self.SetImage(dir)
        self.BrowseDirIndex, item = next(((i,x) for i,x in enumerate(self._currentPlaylistDirs)  if x == path(dir)), (None, None))
        self.timer.Stop()
        event.Skip()

    def OnCoverSelect(self, event):
        ''' Selecting cover changes current playlist to new one - songlist is mp3s in cover directory
        # filenames MUST be of the format  'Track - Song.mp3'
        # eg '07 - Viva La Vida.mp3' or '07 - Coldplay - Viva La Vida.mp3'
        '''
        playlist_dict = {}
        self.lb.Clear()

        d = path(self.BrowseDir)
        for file in d.walkfiles('*.mp3'):

            g = path(path.splitpath(file)[1]).stripext().split('-')
            track_info = {}

            # set track parameters ['trackNum', 'title', 'location', 'duration', 'album', 'creator']
            track_info['trackNum'] = g[0].strip()
            track_info['title'] = g[-1].strip()
            track_info['location'] = str(file).strip() # full file path
            track_info['duration'] = track_info['album'] = track_info['creator'] = ''

            playlist_dict[int(g[0]) -1] = track_info

        self.playlist_dict = playlist_dict
        self._currentPlaylistPosition = 0
        self.SetPlaylistBox()
        self.Play()
        self.PL.Create_xspf_Playlist(self.playlist_dict)
        self.timer.Stop()

    def SetPlaylistBox(self):
        [self.lb.Insert(self.playlist_dict[ID]['title'], ID)  for ID in self.playlist_dict]
        self.lastID = ID
        self.lb.SetSelection(self._currentPlaylistPosition)

    def UpdateDirs(self):
        '''  Update the list of filepaths of directories that contain mp3 files'''
        playdirs = []
        d = path(self.mp3root)
        for dir in  d.walkdirs():
            if  path(dir).files('*.mp3'):
                playdirs.append(str(dir).replace('\\', '/'))  # maintain path separators as forward slashes on windows

        f = open(self.playdirs_rc, 'w')
        pickle.dump(sorted(playdirs), f)
        f.close()
        print "Done ..."

    def SetImage(self, img_path):
        img =  path.joinpath(path(img_path), "cover.jpg") # TODO - more options
        if path.exists(img):
            image = wx.Image(img).Rescale(self.bmp_size[0], self.bmp_size[1])
            bmp = wx.BitmapFromImage(image)
            self.coverart.SetBitmapLabel(bmp)
        else:
            print "AudioPlayerPanel: Image error"
            self.SetImage(self.rcdir)

    def dict_path(self):
        #self._currentPlaylistPosition = 0
        filepath=  self.playlist_dict[self._currentPlaylistPosition]['location']
        dir, file = os.path.split(filepath)
        return dir, file, filepath

    def EvtListBox(self, event):
        '''"AudioPlayerPanel: ", event.GetSelection(), event.GetString()'''
        self._currentPlaylistPosition = event.GetSelection()
        # Use try except  here so can always ignore the error generated when
        # a new playlist is created  - normally considered bad programming practice :(
        try:
            self.Play()
        except:
            pass

    def Start(self):
        '''On startup read last played song ID and start playing it'''
        # TODO add time info
        try:
            f = open(self.playpos_rc, 'r')
            self._currentPlaylistPosition = pickle.load(f)
            f.close()
        except:
            print 'AudioPlayerPanel:Unable to read playlist position. Setting to start...'
            self._currentPlaylistPosition = 0

        self.Play()

    def SaveStatus(self):
        f = open(self.playpos_rc, 'w')
        pickle.dump(self._currentPlaylistPosition, f)
        f.close()

    def Play(self):
        self.lb.SetSelection(self._currentPlaylistPosition)
        junk, junk, target =  self.dict_path()
        self.tgt = target   # self.tgt saves target to revert to on non-selection of coverart
        self.mplayer.play(target)
        self.SaveStatus()

    def OnPlayPrev(self, event):
        '''AudioPlayerPanel: Play Previous Song'''
        self._currentPlaylistPosition -=1
        if self._currentPlaylistPosition < 0:
            self._currentPlaylistPosition = self.lastID
        self.Play()

    def OnPlayPause(self, event):
        '''AudioPlayerPanel: Toggle Play Pause'''
        if self.mplayer.GetPlayStatus():
            self.mplayer.pause()
        else:
            self.Play()

    def OnPlayNext(self, event):
        '''AudioPlayerPanel:Play Next Song'''
        self._currentPlaylistPosition +=1
        if self._currentPlaylistPosition > self.lastID:
            self._currentPlaylistPosition = 0
        self.Play()

    def OnSkipRev(self, event):
        '''AudioPlayerPanel: Skip thru backwards'''
        self.mplayer.seek(-5)

    def OnSkipFwd(self, event):
        '''AudioPlayerPanel: Skip thru forwards'''
        self.mplayer.seek(5)

    def OnUpDir(self, event):
        ''' Change up firstly by artist album, then just artist alphabetical group '''
        # TODO - works as intended - but is a little ugly, fragile, and inflexible
        #  BUG - Error on up from @ directories
        cpd = self._currentPlaylistDirs

        up = lambda dir: path(dir).splitpath()[0]

        def dirinfo(dir):
            dirpaths = sorted(path(up(dir)).dirs())
            dirpaths = list(i.replace('\\', '/') for i in dirpaths)
            if len(path(path(self.mp3root).splitpath()[0]).splitall()) > len(path(path(dirpaths[0]).splitpath()[0]).splitall()):
                print "too far"
                return
            index, item = next(((i, x) for i, x in enumerate(dirpaths) if x == path(dir)), (None, None))
            return index, item, dirpaths

        if self.BrowseDirIndex == 0 or self.BrowseDirIndex == 1:
            self.BrowseDirIndex = len(cpd) -1
            self.BrowseDir = cpd[self.BrowseDirIndex]
            self.SetImage(self.BrowseDir)
            self.timer.Start(self.revert_time)
            return
        else:
            index, item, dirpaths = dirinfo(up(self.BrowseDir))          # returned info of the up directory

        if not index == 0: # if equals zero then we are at first dir of that alpha group
            index -= 1
            index2 = index
            dir = str(path(dirpaths[index])).replace('\\', '/')
            index, item = next(((i, x) for i, x in enumerate(cpd) if x.startswith(dir)), (None, None))
            if item == None:
                dir = path(dirpaths[index2]).dirs()[0]
                index, item = next(((i, x) for i, x in enumerate(cpd) if x.startswith(dir)), (None, None))
        else:
            # At top of alpha group so change up alphabetically ie D, C, B, A, @, Z, Y...
            index, item, dirpaths = dirinfo(up(item))
            index -= 1
            updir = str(sorted(path(dirpaths[index]).dirs())[0]).replace('\\', '/')
            index, dir = next(((i, x) for i, x in enumerate(cpd) if x.find(updir) ==0), (None, None))

        self.BrowseDirIndex = index
        self.BrowseDir = cpd[self.BrowseDirIndex]
        self.SetImage(self.BrowseDir)
        self.timer.Start(self.revert_time)

    def OnDownDir(self, event):
        '''Change down one *directory*. Increment is in Alphabetical artist groups ie A, B, C etc'''

        len_x = len(path(self.mp3root).splitall())
        # split to directory past mp3root
        split_path = path(self._currentPlaylistDirs[self.BrowseDirIndex]).splitall()[:len_x]
        # change directory by +1 (base dirs are A, B, C etc)
        # TODO - needs to be able to cope with non-existing directories esp A and Z -  done but not fully checked
        # Note - dont have empty directories
        # Note: A= 65, Z=90

        def down_and_check():

            split_path[-1] = chr(ord(split_path[-1]) +1)
            if  ord(split_path[-1]) > 90:
                split_path[-1] = '@'

            # rejoin path elements
            self.diry = path.joinpath(split_path[0], '/'.join(split_path[1:]))
            if not  path.exists(self.diry):
                split_path[-1] = chr(ord(split_path[-1]) +1)
                down_and_check()
            return path.exists(self.diry)

        while 1:
            if down_and_check():
                # find index and directory  from possibles in list self._currentPlaylistDirs
                self.BrowseDirIndex, dir = next(((i, x) for i, x in enumerate(self._currentPlaylistDirs) if x.find(self.diry) ==0), (None, None))
                break
            else:
                down_and_check()

        self.BrowseDir = self._currentPlaylistDirs[self.BrowseDirIndex]
        self.SetImage(self.BrowseDir)
        self.timer.Start(self.revert_time)

    def OnNextDir(self, event):
        ''' Go to the 'next' directory as determined by the file referenced in self._currentPlaylistDirs. Note that this file must be updated manually at this time'''
        if len(self._currentPlaylistDirs) - self.BrowseDirIndex == 1:
            self.BrowseDirIndex = 0
        else:
            self.BrowseDirIndex += 1

        self.BrowseDir = self._currentPlaylistDirs[self.BrowseDirIndex]
        self.SetImage(self.BrowseDir)
        self.timer.Start(self.revert_time)

    def reSkin(self, color):
        # re-skin me
        self.SetBackgroundColour(getattr(self.settings.skin, color +'_bgColor'))

        # TODO add these ListBox elements to skin file
        if color == 'night':
            self.lb.SetBackgroundColour((0,0,0)) # Black
            self.lb.SetForegroundColour(( 240, 240, 240))
        elif color == 'day':
            self.lb.SetBackgroundColour((140, 140, 140))
            self.lb.SetForegroundColour((0,0,255))

        self.Refresh()

    def updateProgressBar(self, progressValue):
        """This function called from player class to update the progress bar"""
        self.gauge1.SetValue(progressValue)

    def SetSongLength(self, lengthSec):
        """This function called from player class to set progress bar length in seconds."""
        self.gauge1.SetRange(lengthSec)

    def CloseMplayer(self):
        self.mplayer.closeMplayer()


