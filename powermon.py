#! /usr/bin/env python

'''
Monitor the 'AC power' (actually it is DC in the car). If power goes off then gracefully
stop Player etc (so that song playing is remembered) and then turn off the Laptop.
'''
#TODO
#Test  with hibernate
#Linux version

test_mode = False		# If in test mode wont  actually shutdown
timeout_delay = 5   	#Shutdown wont commence unless power is off longer than this value
shutdown_delay = '4'  	# This is the windows shutdown delay  once shutdown started
have_dock = False    	# just adds some delay


import time, os, sys, threading, wx, subprocess

#if sys.platform == 'linux2':
    
if sys.platform == 'win32':
    from ctypes import windll, byref, Structure
    from ctypes import c_ushort, c_ubyte, c_ulong, c_char
    
    kernel32 = windll.kernel32
    BYTE = c_ubyte
    DWORD = c_ulong

    class DStructure(Structure):
        def dump(self):
            #print "%s" % (self.__class__.__name__)
            for name, fmt in self._fields_:
                val = getattr(self, name)
                if isinstance(val, int):
                    if name == "ACLineStatus":
                        return val

    class SYSTEM_POWER_STATUS(DStructure):
        _fields_ = [("ACLineStatus", BYTE),
            ("BatteryFlag", BYTE),
            ("BatteryLifePercent", BYTE),
            ("Reserved1", BYTE),
            ("BatteryLifeTime", DWORD),
            ("BatteryFullLifeTime", DWORD)]
        
class PowerMon(threading.Thread):
    def __init__(self, parent):
        threading.Thread.__init__(self)
        
        ## initialise python dictionary to store fail times
        ## should never be more keys than the value of the timeout_delay 
        self.fail_time = {0:int(time.clock()),}
        
        self.ftcount = ftcount = self.stop = 0
        self.parent = parent
        if sys.platform == 'linux2':
            subprocess.call(['upower', '--enumerate'])
            self.proc = subprocess.Popen('upower --monitor-detail', shell=True, stdout=subprocess.PIPE, )
        
    def run(self):
        #print "start powermon"
        self.start_monitor(self.ftcount)
        
    def start_monitor(self, ftcount):
        # A bit of a convuluted way to check if power is off for more than just a glitch
        # ie like what might happen as volts drop on engine start
        # using time.clock() because threading.Timer()  is no easier

        if not self.stop:
            if sys.platform == 'linux2':
                while self.Monitor_AC_Linux():
                    print "Power OK"
                else:
                    print "Power OFF"
                    self.Shutdown()

                        
            elif sys.platform == 'win32': 
                while self.Monitor_AC():
                    #print "Power OK"
                    self.fail_time[1] = int(time.clock())
            
                else:
                    print "Power OFF"
                    ftcount += 1 #  increment the fail time counter
                    self.fail_time[ftcount] = int(time.clock())
                    tlfail = self.fail_time[ftcount] - self.fail_time[1]
                    #print "Shutting down at 10. Now = %s " %  tlfail
                    if  tlfail >= timeout_delay:
                        self.Shutdown()
                    else:
                        # continue monitoring power 
                        self.start_monitor(ftcount)

    def Monitor_AC(self, AC=1):
        # read the AC power status using kernel32.dll
        while AC:
            sps = SYSTEM_POWER_STATUS()
            kernel32.GetSystemPowerStatus(byref(sps))
            AC = sps.dump()
            time.sleep(1)  # need some delay or use too much CPU in While loop
            if self.stop:
                AC = 0
            #print AC, self.stop
            return AC
     
    def Monitor_AC_Linux(self, AC=1):
        #proc = subprocess.Popen('upower --monitor-detail', shell=True, stdout=subprocess.PIPE, )
        print "monitoring"
        while AC:
            output = self.proc.stdout.readline()
            #print output
            d = output.strip().split(':')
            if d[0] == 'on-battery':
                status = d[1].strip()
                if status == 'yes':
                    print' power has failed - on battery'
                    AC = 0
                else:
                    AC = 1


            print 'AC = ', AC
            return AC
                
    def Stop(self):
        self.stop = 1

    def Shutdown(self):
        # 
        if have_dock:
            print "AC failed -waiting ten for dock to settle"
            time.sleep(10) # add this delay because of Lenovo Dock craziness
        else:
            print "AC failed"
        
        # Initiate shutdown
        print "shuting down in %s seconds" % shutdown_delay
        if test_mode:
            print "NOT SHUTTING DOWN"
            print "Make test_mode = FALSE at top of file to enable shutdown"
        else:
            if sys.platform == 'linux2':
                time.sleep(int(shutdown_delay))
                subprocess.call(['/sbin/shutdown', '-h' , 'now'], )
            elif sys.platform == 'win32':
                #subprocess.Popen("shutdown -f -s -t %s" % shutdown_delay)
                subprocess.call(["taskkill", "/im", "xTouchMon.exe", "/f"],)
                subprocess.call(["shutdown", "-s", "-t", shutdown_delay], )
                #Shutdown application
                self.parent.Quit()                

            
