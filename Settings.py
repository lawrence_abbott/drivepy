# TuxTruck Skin Manager
# Time-stamp: "2008-05-12 15:11:29 jantman"
# $Id: TuxTruck_Settings.py,v 1.6 2008/05/12 19:10:43 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

import os, sys
import xml.etree.ElementTree as ElementTree
 
#local imports
import Utility, Skin

class Settings:
    """"
    This class should be instantiated at the beginning of the main application. It provides
    an interface to all program setting-related data. Child classes handle reading, writing,
    and accessing setting data. I.e., to get the current skin file, we have a child class "skin",
    which is an instance of TuxTruck_SkinManager, as access that value as settings.skin.currentSkinFile.
    Child classes should read in the current (or default) settings at start.
    NOTE: Programmers should *NOT* modify variables directly. If it doesn't have a set() method,
    leave it alone.
    WARNING: Setting changes are not atomic. After running a set() method, you should run settings.writeSettings()
    which writes out the current settings from all child classes to their respective files. NOTE that this is 
    another reason not to modify variables directly. ALL variables are written to config files (i.e. on writeSettings(),
    every config file is totally re-written). If things get inadvertently changed (i.e. using = instead of ==), those 
    changes WILL be permanently written to the config files.
    This should have a child class for every category of settings, i.e. skins, navigation, phone, podcasts, etc.
    NOTE: ALL config files are stored in ~/.tuxtruck/
    """

    def __init__(self, module):
        "We instantiate all child classes, which will load in all settings and make them available."

        # load all settings related to the skin/GUI, from the SkinManager class
        
        # location of user  platform specific items
        self.userdir = os.path.expanduser('~')
        
        if sys.platform == 'linux2':
            self.module_path = '.'+module
        elif sys.platform == 'win32':
            self.module_path = module
        else:
            self.module_path = module
            print "Platform unidentified"
            
        self.rcdir = os.path.join(self.userdir,  self.module_path)
        if not os.path.exists(self.rcdir):
            os.makedirs(self.rcdir)  # TODO - get this outta here

        defaultSkinFile = os.path.join(self.rcdir, "settings/skins/defaultSkin.xml")
        audioSettingsFile = os.path.join(self.rcdir, "settings/audio.xml")
        
        self.audio = Settings_Audio(self, audioSettingsFile)
        self.skin = SkinManager(self, defaultSkinFile)
        
        
class Settings_Audio:
    """
    This class handles reading and writing all of the settings related to audio playing.
    """
    
    def __init__(self, parent, file):
        """
        Here, we get the default skin name from settings, then load that file.
        This must happen before we build any of the GUI.
        """
        self.loadSkin(parent, file) # load the file

    def loadSkin(self, parent, file):
        """
        This function loads a new skin by reading and parsing the file, and then
        updating the variables in this class. The skin is specified by a file, in the
        path specified in the class documentation. It only updates the variables here,
        and doesn't make any changes to the GUI.
        NOTE: The size of buttons is exactly the size of the image used for the button.
        """
        
        audioTree = ElementTree.parse(file).getroot()

        self.mp3root = audioTree.findtext("mp3root")
        self.playlistroot = audioTree.findtext("playlistroot")
        self.podcastroot = audioTree.findtext("podcastroot")
        
        
class SkinManager:
    """
    This class provides an interface to all visual (skin) related information.
    It holds variables with all of the information needed to customize the GUI,
    and provides methods to update those variables from external files.
    This class should be instantiated at the start of the application. It will 
    then read in a skin data file, update the variables, and they'll be available 
    to the main program.
    NOTE: This is instantiated by the TuxTruck_Settings class, which provides an accessor
    and modifiers for all settings (easy access, like settings.skin.currentSkinFile).
    WARNING: Skin files should be static, i.e. the program should *not* modify them.
    Skin files are stored in ~/.tuxtruck/skins/
    A few very common values, such as skin name, file, and primary skin colors are stored
    locally. Everything else is subclassed - ex. there's a analogclock class that handles
    settings for the analog clock, which can then be references like: settings.skin.analogclock.day_bgColor
    """

    # General information on the current skin
    # leave these as empty strings until we put something in them
    _currentSkinName = "" # name of the current skin
    _currentSkinFile = "" # filename of the current skin, 
    _buttonImagePath = "" # path, relative to ~/.tuxtruck/skins/ to the images for this skin
    
    def __init__(self, parent, skinFile):
        """
        Here, we get the default skin name from settings, then load that file.
        This must happen before we build any of the GUI.
        """
        #get the default skin name and file from settings
        #load the skin file. make changes to default, anything not specified stays default

        self.parent = parent

        self.loadSkin(skinFile) # Load MY MAIN skin
        self.butn = Skin.Button(self, skinFile) # load the buttons information
        self.digiClock = Skin.DigitalClock(self, skinFile) # load the buttons information
        self.anaClock = Skin.AnalogClock(self, skinFile) # load the buttons information
        self.inTherm = Skin.InternalThermo(self, skinFile) # load the internal clock skin
        self.exTherm = Skin.ExternalThermo(self, skinFile) # load the external clock skin

    def loadSkin(self, file):
        """
        This parses your skin file for global settings, such as name,
        background and foreground colors, size, button info, etc.
        """
        
        skinTree = ElementTree.parse(file).getroot()

        # parse the window information
        windowTree = skinTree.find('window')
        self.topWindowCentered = windowTree.findtext("is_centered")
        self.topWindowSize = (int(windowTree.findtext("width")), int(windowTree.findtext("height")))
        self.topWindowPos = (int(windowTree.findtext("pos_X")), int(windowTree.findtext("pos_Y")))
        # TODO: add the centered part

        # parse the main colors
        self.day_bgColor = Utility.str2tuple(windowTree.findtext("day_bgColor"), "window.day_bgColor")
        self.day_fgColor = Utility.str2tuple(windowTree.findtext("day_fgColor"), "window.day_fgColor")
        self.day_toolbarColor = Utility.str2tuple(windowTree.findtext("day_toolbarColor"), "window.day_toolbarColor")
        self.day_textColor = Utility.str2tuple(windowTree.findtext("day_textColor"), "window.day_textColor")
        self.day_highlightColor = Utility.str2tuple(windowTree.findtext("day_highlightColor"), "window.day_highlightColor")
        self.night_bgColor = Utility.str2tuple(windowTree.findtext("night_bgColor"), "window.night_bgColor")
        self.night_fgColor = Utility.str2tuple(windowTree.findtext("night_fgColor"), "window.night_fgColor")
        self.night_toolbarColor = Utility.str2tuple(windowTree.findtext("night_toolbarColor"), "window.night_toolbarColor")
        self.night_textColor = Utility.str2tuple(windowTree.findtext("night_textColor"), "window.night_textColor")
        self.night_highlightColor = Utility.str2tuple(windowTree.findtext("night_highlightColor"), "window.night_highlightColor")

        # parse global information
        globalTree = skinTree.find('globalSkin')
        self._currentSkinName = globalTree.findtext("skinName")
        self._currentSkinFile = file
        self._buttonImagePath = os.path.join(self.parent.module_path, globalTree.findtext("buttonPath"))
