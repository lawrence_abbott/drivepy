#! /usr/bin/env python
# Clock panel for home view
# Time-stamp: "2008-05-15 16:29:20 jantman"
# $Id: HomePanel_Clock.py,v 1.11 2008/05/15 20:32:43 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

# the clocks came from http://xoomer.alice.it/infinity77/main/SpeedMeter.html
# TODO: add temperature display and "alert" box for important text
# TODO have LED background set other than white. How?
# TODO LED code needs work, and is commented out - not sure of usefullness of converting text to 7 seg display anyway (other than being cool to be able to do it)

import wx
from wx.lib import analogclock
#import LEDCtrl as led
#import time

class HomePanel_Clock(wx.Panel):
    """
    This panel shows the clocks for the main "home" mode.
    """

    def __init__(self, parent, id):
        """
        TODO: This needs to be documented.
        """
        wx.Panel.__init__(self, parent, id)

        self.settings = parent.settings
        
        # setup the panel
        x, y = self.settings.skin.topWindowSize
        self.SetPosition(wx.Point(0,0)) # set the main window position
        self.SetSize(wx.Size(x, y - self.settings.skin.butn.height)) # set the main window size
        #self.SetBackgroundColour(self.settings.skin.day_bgColor)

        # analog clock
        # TODO remove hardcoding of pos and size
        self.clock = analogclock.AnalogClock(self)
        self.clock.SetSize((300,300))
        self.clock.SetPosition((265,50))
        
        # for LED setting
        """
        style = led.LED_ALIGN_CENTRE| \
                led.LED_AGG|          \
                led.LED_ALLOW_COLONS| \
                led.LED_SLANT
        """
        # digital clock 
        """
        # TODO have analog/digital selection option
        self.myled = led.LEDCtrl(self)
        self.myled.SetLedStyle(style)
        self.myled.Freeze()
        self.myled.SetWindowStyle(wx.SUNKEN_BORDER)
        self.myled.SetSize((400, 80))
        self.myled.SetPosition((200,330))
        self.myled.SetDigits(6)
        self.myled.SetSize(self.size)
        self.myled.Thaw()
        """
        # setup INTERNAL thermometer
        """
        self.inTherm = led.LEDCtrl(self)
        self.inTherm.SetLedStyle(style)
        self.inTherm.Freeze()
        self.inTherm.SetWindowStyle(wx.SUNKEN_BORDER)
        self.inTherm.SetSize((125,50))
        self.inTherm.SetPosition((25,320))
        self.inTherm.SetDigits(5)
        self.inTherm.SetValue("23QC") # TODO: update this from the thermometer
        self.inTherm.Thaw()
        """
        # setup EXTERNAL thermometer
        """
        self.exTherm = led.LEDCtrl(self)
        self.exTherm.SetLedStyle(style)
        self.exTherm.Freeze()
        self.exTherm.SetWindowStyle(wx.SUNKEN_BORDER)
        self.exTherm.SetSize((125, 50))
        self.exTherm.SetPosition((650,320))
        self.exTherm.SetDigits(5)
        self.exTherm.SetValue("35QC") # TODO: update this from the thermometer
        self.exTherm.Thaw()
        """
        # init the timer
        """
        self.tc = -1
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.OnTimer)
        self.StartTimer()
        """
        self.Hide()


        
    def reSkin(self, color):
        """ This is called to re-load the skin settings."""
        
        self.SetBackgroundColour(getattr(self.settings.skin, color +'_bgColor'))
        
        if color == 'day':
            # set day scheme
            # analog clock
            self.clock.SetHandBorderColour(self.settings.skin.anaClock.day_handColor)
            self.clock.SetHandFillColour(self.settings.skin.anaClock.day_handColor)
            self.clock.SetShadowColour(self.settings.skin.anaClock.day_shadowColor)
            self.clock.SetBackgroundColour(self.settings.skin.anaClock.day_bgColor)
            self.clock.SetTickBorderColour(self.settings.skin.anaClock.day_tickColor)
            self.clock.SetTickFillColour(self.settings.skin.anaClock.day_tickColor)
            self.clock.SetFaceBorderColour(self.settings.skin.anaClock.day_faceBorderColor)
            self.clock.SetFaceFillColour(self.settings.skin.anaClock.day_faceColor)
           
            # LED clock
            """
            self.myled.SetForegroundColour(self.settings.skin.digiClock.day_fgColor)
            self.myled.SetBackgroundColour(self.settings.skin.digiClock.day_bgColor)
            self.myled.SetFadeColour(self.settings.skin.digiClock.day_fadeColor)
            self.myled.SetFadeFactor(self.settings.skin.digiClock.fadeFactor)
            """
            # INTERNAL thermometer
            """
            self.inTherm.SetForegroundColour(self.settings.skin.inTherm.day_fgColor)
            self.inTherm.SetBackgroundColour(self.settings.skin.inTherm.day_bgColor)
            self.inTherm.SetFadeColour(self.settings.skin.inTherm.day_fadeColor)
            self.inTherm.SetFadeFactor(self.settings.skin.inTherm.fadeFactor)
            """
            # EXTERNAL thermometer
            """
            self.exTherm.SetForegroundColour(self.settings.skin.exTherm.day_fgColor)
            self.exTherm.SetBackgroundColour(self.settings.skin.exTherm.day_bgColor)
            self.exTherm.SetFadeColour(self.settings.skin.exTherm.day_fadeColor)
            self.exTherm.SetFadeFactor(self.settings.skin.exTherm.fadeFactor)
            """
        else:
            # set night scheme
            # analog clock
            self.clock.SetHandBorderColour(self.settings.skin.anaClock.night_handColor)
            self.clock.SetHandFillColour(self.settings.skin.anaClock.night_handColor)
            self.clock.SetShadowColour(self.settings.skin.anaClock.night_shadowColor)
            self.clock.SetBackgroundColour(self.settings.skin.anaClock.night_bgColor)
            self.clock.SetTickBorderColour(self.settings.skin.anaClock.night_tickColor)
            self.clock.SetTickFillColour(self.settings.skin.anaClock.night_tickColor)
            self.clock.SetFaceBorderColour(self.settings.skin.anaClock.night_faceBorderColor)
            self.clock.SetFaceFillColour(self.settings.skin.anaClock.night_faceColor)

            # LED clock
            """
            self.myled.SetForegroundColour(self.settings.skin.digiClock.night_fgColor)
            self.myled.SetBackgroundColour(self.settings.skin.digiClock.night_bgColor)
            self.myled.SetFadeColour(self.settings.skin.digiClock.night_fadeColor)
            self.myled.SetFadeFactor(self.settings.skin.digiClock.fadeFactor)
            """
            # INTERNAL thermometer
            """
            self.inTherm.SetForegroundColour(self.settings.skin.inTherm.night_fgColor)
            self.inTherm.SetBackgroundColour(self.settings.skin.inTherm.night_bgColor)
            self.inTherm.SetFadeColour(self.settings.skin.inTherm.night_fadeColor)
            self.inTherm.SetFadeFactor(self.settings.skin.inTherm.fadeFactor)
            """
            # EXTERNAL thermometer
            """
            self.exTherm.SetForegroundColour(self.settings.skin.exTherm.night_fgColor)
            self.exTherm.SetBackgroundColour(self.settings.skin.exTherm.night_bgColor)
            self.exTherm.SetFadeColour(self.settings.skin.exTherm.night_fadeColor)
            self.exTherm.SetFadeFactor(self.settings.skin.exTherm.fadeFactor)
            """

        self.Refresh()
        
"""
    def StartTimer(self):
        ''' This is used to start the timer for the clock.'''
        self.timer.Start(500)

    def StopTimer(self):
        ''' This is used to stop the timer for the clock.'''
        self.timer.Stop()

    def OnTimer(self, evt):
        ''' This handles updating the clocks.'''
        self.tc = -self.tc
        t = time.localtime(time.time())
        if self.tc > 0:
            st = time.strftime("%H:%M:%S", t)
        else:
            st = time.strftime("%H%M%S", t)
        #self.myled.SetValue(st)
        
    def UpdateInternalTemp(self, degrees):
        '''
        This function is called externally to update the internal temperature display. It should be called with a string of at most 3 digis, followed by the letter Q representing the degrees symbol, and optionally followed by either C or F.
        '''
        self.intTherm.SetValue(degrees+ 'QF')
        # TODO: this is set to farenheit only, need to check what our default type is

    def UpdateExternalTemp(self, degrees):
        '''
        This function is called externally to update the external temperature display. It should be called with a string of at most 3 digis, followed by the letter Q representing the degrees symbol, and optionally followed by either C or F.
        '''
        self.exTherm.SetValue(degrees+"QF")
        # TODO: this is set to farenheit only, need to check what our default type is
"""