#! /usr/bin/env python
# OBD main frame
# Time-stamp: "2008-05-27 13:10:50 jantman"
# $Id: OBDPanel.py,v 1.8 2008/05/27 18:23:08 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

import wx

class OBDPanel(wx.Panel):
    """
    This is the top-level panel for OBD functions.
    """

    def __init__(self, parent, id):
        """
        Init function for the OBD panel. Sets everything up.
        """
        self.parent = parent
        self.settings = parent.settings
        
        wx.Panel.__init__(self, parent, id)

        # setup the panel
        self.SetPosition(wx.Point(0, 0))
        x, y  =  self.settings.skin.topWindowSize     
        self.SetSize(wx.Size(x, y - self.settings.skin.butn.height))

        # test button
        self.button1 = wx.Button(self, -1, "OBD")
        self.button1.SetPosition(wx.Point(400, 100))
        self.button1.SetSize((50, 50))
        self.button1.Bind(wx.EVT_BUTTON, self.test) 

        self.Hide()
        
    def reSkin(self, color):
        # re-skin me
        self.SetBackgroundColour(getattr(self.settings.skin, color +'_bgColor'))
        self.Refresh()
        
    def test(self, event):
        print "Testing OBD..."

