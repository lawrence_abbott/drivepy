#! /usr/bin/env python
# Playlist
# Time-stamp: "2008-05-27 16:23:58 jantman"
# $Id: Playlist.py,v 1.7 2008/05/27 20:23:34 jantman Exp $
#
# Copyright 2008 Jason Antman. Licensed under GNU GPLv3 or latest version (at author's discretion).
# Jason Antman - jason@jasonantman.com - http://www.jasonantman.com
# Project web site at http://www.jasonantman.com/tuxtruck/

# Modified to use 'standard' xspf playlist format as used by amarok, winamp, vlc etc


from xml.etree import ElementTree
import os, re, urllib, sys


class Playlist():
    """
    This handles *all* playlist-related functions. Specifically, this loads playlists and provides methods parse and write the playlist XML files.
    Using 'standard' xspf playlist format as used by amarok, vlc and others
    """
    #  last playlist  is saved and loaded from/to currentPlaylist.xspf - on new install a file from amarok or vlc will do to kick start.
    current_file = "currentPlaylist.xspf"
    
    playlistROOT = None         # this will be the elementtree root node
    playlistTREE = None         # this will be the full elementtree tree
    playlist_root_path = ""     # as found in settings/audio
    songsROOT = None        # this is the elementtree root for the songs entries
    currentPlaylistPosition = -1 

    def __init__(self, parent, playlistroot):
        
        self.parent = parent
        self.playlist_root_path = playlistroot
        
        if sys.platform == 'linux2':
            self.pre = 'file://'
        elif sys.platform == 'win32':
            self.pre = 'file:///'

    def Read_xspf_Playlist(self, file_name=current_file):
        """
        This function reads in a playlist and parses it. After calling this, you can get the individual entries. file_path
        is the *absolute* path to the playlist file.
        """
        
         #add namespace to front of each tag 
        def ns(tag):
            return '{%s}%s' % ('http://xspf.org/ns/0/', tag)
            
        # create dictionary to hold playlist info
        playlist_dict = {}
        #print self.playlist_root_path
        self.playlist_file_path = os.path.join(self.playlist_root_path, file_name) 
        playlistTREE = ElementTree.parse(self.playlist_file_path)
        playlistROOT = playlistTREE.getroot()
        songsROOT = playlistROOT.find(ns('trackList'))
     
        # get info for each track in playlist and store in dictionary
        for index, item  in enumerate(songsROOT.getiterator(ns('track'))):
            track_info = {}

            for atr in ['trackNum', 'title', 'location', 'duration', 'album', 'creator']:
                if atr == 'location':
                    val = urllib.unquote(item.findtext(ns(atr))).replace(self.pre, "")
                else:
                    val = item.findtext(ns(atr))
                track_info[atr] = val
            
            playlist_dict[index] = track_info

        return playlist_dict
        
        
    def Create_xspf_Playlist(self, playlist_dict, file_name=current_file):
        ''' Create the playlist for saving to file'''
        # In progress
        # Note: added newlines are for formating purposes only
        
        outfile = os.path.join(self.playlist_root_path, file_name)
        album = songtime = ''

        def addTag(tag, text):
            builder.start(tag, {})
            builder.data(text)
            builder.end(tag)
            builder.data('\n')
            
        builder = ElementTree.TreeBuilder()
        builder.start("playlist", {'version': '1', 'xmlns': 'http://xspf.org/ns/0/'})
        builder.data("\n")
        builder.start("trackList", {})
        builder.data("\n")

        for ID in playlist_dict:
            
            title = playlist_dict[ID]['title']
            track = playlist_dict[ID]['location']
            artist = playlist_dict[ID]['creator']
            trackNum = str(ID)
            
            builder.start('track', {})
            builder.data('\n')
            
            addTag('location', self.pre + urllib.quote(track, '/:'))
            addTag('title', title)        # song title
            addTag('creator', artist)    # artist
            addTag('album', album)       # name
            addTag('trackNum', trackNum)  # integer
            addTag('duration', songtime) # millisecs
            
            builder.end('track')
            builder.data('\n')

        builder.end("trackList")
        builder.data("\n")
        builder.end("playlist")
        elem = builder.close()

        tree = ElementTree.ElementTree(elem)

        tree.write(outfile,  encoding='UTF-8')
                
        
        
####################################################################################
##
##                            TODO Everything below here... may or may not be required
##
####################################################################################

    def IsInPlaylist(self, filepath):
        """
        Returns TRUE if the playlist located at playlistFile contains a (song) entry containing exactly filepath as its "filepath" field content. Otherwise returns false.
        """

        # TODO: create a GetSongByName() function

        songsIter = self.songsROOT.getiterator()
        
        for entry in songsIter:
            if entry.findtext('filepath') == filepath:
                return True
        return False

    def WriteCurrentPlaylist(self):
        """
        This function WRITES the current playlist changes to the playlist file. It's mainly used when building a new playlist, to write the complete tree, or when updating the rank of a file. Path is absolute.
        """
        #print "Writing playlist to "+self.current_file # debug

        # wrap it in an ElementTree instance, and save as XML
        tree = ElementTree(self.playlistROOT)
        tree.write(self.current_file, encoding='UTF-8')


    def GetEntryByPos(self, pos):
        """
        This gets an entry by position number in the playlist. Takes the integer position as an argument, returns a 3-tuple (pos (int), filepath (string), displayName (string)). File path is relative to MP3_ROOT.
        """

    def GetNextEntry(self):
        """
        This function returns the next entry in the playlist. It returns a 3-tuple (pos (int), filepath (string), displayName (string)). filepath is relative to MP3_ROOT.
        """

    def AddEntry(self, filepath, displayName, title, artist, album, genre):
        """
        This function adds an entry to the current playlist. All entry information is specified as arguments - please see the playlist documentation for an explanation of the fields. Any fields that do not have an appropriate value should be sent as an empty string - "". WriteCurrentPlaylist() must be called to write out the playlist to the file.
        """

        #print "Adding entry with displayName "+displayName+" and filepath="+filepath+" to "+self.current_file # DEBUG

        entry = SubElement(self.songsROOT, "playlist_entry")
        filepathElem = SubElement(entry, "filepath")
        filepathElem.text = filepath
        displayNameElem = SubElement(entry, "displayName")
        displayNameElem.text = displayName
        if title != "":
            titleElem = SubElement(entry, "title")
            titleElem.text = title
        if artist != "":
            artistElem = SubElement(entry, "artist")
            artistElem.text = artist
        if album != "":
            albumElem = SubElement(entry, "album")
            albumElem.text = album
        if genre != "":
            genreElem = SubElement(entry, "genre")
            genreElem.text = genre

    def CreateBlankPlaylist(self, filepath, name, ftype):
        """
        This function creates a skeleton of a blank playlist, ready for adding entries to (for use when building playlists from disk files). 
        Entries are added with AddEntry(). When finished, it is written to disk with WriteCurrentPlaylist(). Arguments are filepath, name
        and type as seen in the playlist documentation. Path is absolute.
        """
        # TODO: also include playlist name, type, etc.

        self.current_file = filepath # where we'll save to

        self.playlistROOT = ET.Element("playlist", version="1", xmlns="http://xspf.org/ns/0/") # create the root node

        trackListElem = ET.SubElement(self.playlistROOT, "trackList")
        #typeElem.text = ftype
        nameElem = ET.SubElement(self.playlistROOT, "location")
        nameElem.text = name

        self.songsROOT = ET.SubElement(self.playlistROOT, "track")

        print "CreateBlankPlaylist name="+name+" type="+ftype+" path="+filepath # DEBUG
        self.WriteCurrentPlaylist()
        #dump(self.playlistROOT)

    def ReadOrCreatePlaylist(self, filepath, name, type):
        """
        This function is used by playlist builders. If the specified path exists, it reads it in. If not, it creates it. Filepath is an absolute path.
        """

        #print "ReadOrCreatePlaylist name="+name+" type="+type+" path="+filepath # DEBUG

        if os.path.exists(filepath):
            self.ReadPlaylist(filepath)
        else:
            self.CreateBlankPlaylist(filepath, name, type)

    def ChangeRank(self, rank):
        """
        This function changes the integer rank of the current file. Rank should be an int, either -1, 0, or 1.
        """

        # TODO: implement this
        
        WriteCurrentPlaylist() # write out the changes immediately.

    def MakePlaylistFilename(self, s):
           """
           This function formats a string (such as an artist name or album name) as a playlist file. It converts it to a lower case string containing only the characters [a-z],[0-9],"-","_", and ".". It also appends the ".ttpl" extension. A single, double, or back quote is simply removed, everything else invalid is replaced with a dash, and spaces are replaced with underscores.
           """
           s = self.MakeValidFilename(s) # call makeValidFilename to handle most of the replacements
           s = s.lower() # convert to lower case
           s = s+".ttpl" # append ".ttpl"
           return s

    def MakeValidFilename(self, s):
        """
        This functions converts a string to a valid filename. All it does is make sure the string only contains the characters [a-z],[0-9],"-","_", and ".". A single, double, or back quote is simply removed, everything else invalid is replaced with a dash, and spaces are replaced with underscores.
        """
        while s[0] == ".":
            # remove leading dots
            s = s[1:]
        s = re.sub(" +", "_", s) # replace space with _
        s = s.replace("'", "") # replace ' with nothing
        s = s.replace("\"", "") # replace " with nothing
        s = s.replace("`", "") # replace ` with nothing
        # replace everything else with -
        return s
    
